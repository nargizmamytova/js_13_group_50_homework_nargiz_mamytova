import {Component} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent{
  urlSidebar = 'https://st3.depositphotos.com/11433382/14401/i/600/depositphotos_144011369-stock-photo-popcorn-and-movie-clapper-board.jpg'
}
