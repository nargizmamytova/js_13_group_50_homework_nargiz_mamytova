import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent{
@Input() Title = 'Cinema for you';
logUrl = 'https://play-lh.googleusercontent.com/vCozIwQ1F7yRdWr9JB5MC0p5BGCpwW3HGNN5TCYO6jNJutvxbxIFfjxvu3Dh8ybic3I=s180-rw'
}
