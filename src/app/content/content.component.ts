import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent{
  urlSidebar = 'https://st3.depositphotos.com/11433382/14401/i/600/depositphotos_144011369-stock-photo-popcorn-and-movie-clapper-board.jpg';
}

