import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cards-film',
  templateUrl: './cards-film.component.html',
  styleUrls: ['./cards-film.component.css']
})
export class CardsFilmComponent  {
  @Input() filmName = '"Ice drive"';
  @Input() picture = 'https://waynedalenews.com/wp-content/uploads/2021/07/Ice-Road-Netflix-Movie.jpg';
  @Input() year = '2021'
}
